# StockApp

This project was generated with Spring Initializer

# To start server

* Install java - version 8
* Install maven - 3.1.1 version

* Run cd Tyre-BE to switch into working Directory

* Run mvn spring-boot:run dev server will start at http://localhost:8080/.

   Swagger Docs available at 'http://localhost:8080/swagger-ui.html'

   H2 console available at 'http://localhost:8080/h2-ui'. Database available at 'jdbc:h2:mem:testdb'
