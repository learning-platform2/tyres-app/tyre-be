package com.stockapp.gettyres.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class TyreControllerAdvise {
	
	@ExceptionHandler
	public Object exceptionHandler(Exception e) {
		Map<String, Object> map=new HashMap<>();
		map.put("message", e.getMessage());
		map.put("time-stamp", new Date());
		map.put("http_status",HttpStatus.NOT_FOUND);
		return new ResponseEntity<Object>(map,HttpStatus.BAD_REQUEST);
	}
}
