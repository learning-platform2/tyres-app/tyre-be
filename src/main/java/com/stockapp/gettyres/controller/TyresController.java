package com.stockapp.gettyres.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stockapp.gettyres.service.TyreService;
import com.stockapp.gettyres.tyredto.TyreDto;
@CrossOrigin(origins="*")
@RequestMapping("/api/v1/tyre")
@RestController
public class TyresController {
	
	@Autowired
	private TyreService tyreService;
	
	@PostMapping("/all/accept")
	public ResponseEntity<?> addTyres(@RequestBody List<TyreDto> tyresDto) {
		return ResponseEntity.ok(tyreService.addTyres(tyresDto));
	}
	
	@PutMapping("/stock")
	public ResponseEntity<?> updateTyre(@RequestBody TyreDto tyreDto) throws Exception{
		System.out.println(tyreDto);
		return ResponseEntity.ok(tyreService.updateTyre(tyreDto));
	}
	
	@DeleteMapping("/stock/{id}")
	public ResponseEntity<?> deleteTyre(@PathVariable int id) throws Exception{
		return ResponseEntity.ok(tyreService.deleteTyre(id));
	}
	
	@PostMapping("/stock")
	public ResponseEntity<?> addTyre(@RequestBody TyreDto tyreDto){
		return ResponseEntity.ok(tyreService.addTyre(tyreDto));
	}
	
	@GetMapping("/stock")
	public ResponseEntity<?> getTyre(@RequestParam int tyreId) throws Exception{
		return ResponseEntity.ok(tyreService.getTyre(tyreId));
		
	}
	@GetMapping("/all")
	public ResponseEntity<?> getAllTyres() throws Exception{
		return ResponseEntity.ok(tyreService.getAllTyres());
		
	}
}
