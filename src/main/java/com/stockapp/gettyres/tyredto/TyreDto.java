package com.stockapp.gettyres.tyredto;

import com.stockapp.gettyres.entity.Tyre;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TyreDto {
	
	private int id;
	private String description;
	private int stockQuantity;
	private float unitPrice;
}
