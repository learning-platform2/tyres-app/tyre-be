package com.stockapp.gettyres.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stockapp.gettyres.entity.Tyre;
import com.stockapp.gettyres.repo.TyreRepo;
import com.stockapp.gettyres.service.TyreService;
import com.stockapp.gettyres.tyredto.TyreDto;

@Service
public class TyreServiceImpl implements TyreService {
	
	public TyreDto convertTyreToTyreDto(Tyre tyre) {
		return TyreDto.builder().id(tyre.getId()).description(tyre.getDescription()).stockQuantity(tyre.getStockQuantity()).unitPrice(tyre.getUnitPrice()).build();
	}
	
	public Tyre convertTyreDtoToTyre(TyreDto tyreDto) {
		return Tyre.builder().id(tyreDto.getId()).description(tyreDto.getDescription()).stockQuantity(tyreDto.getStockQuantity()).unitPrice(tyreDto.getUnitPrice()).build();
	}

	@Autowired
	private TyreRepo tyreRepo;
	
	@Override
	public List<TyreDto> addTyres(List<TyreDto> tyresDto) {
		List<Tyre> tyres=new ArrayList<>();
		List<Tyre> result=tyreRepo.saveAll(tyresDto.stream().map(tt->convertTyreDtoToTyre(tt)).collect(Collectors.toList()));
		
		return result.stream().map(ty->convertTyreToTyreDto(ty)).collect(Collectors.toList());
	}

	@Override
	public TyreDto updateTyre(TyreDto tyreDto) throws Exception {
		Tyre tyre=tyreRepo.findById(tyreDto.getId()).orElseThrow(()->new Exception("Tyre doesn't exist"));
		
		tyre.setDescription(tyreDto.getDescription());
		tyre.setStockQuantity(tyreDto.getStockQuantity());
		tyre.setUnitPrice(tyreDto.getUnitPrice());
		return convertTyreToTyreDto(tyreRepo.saveAndFlush(tyre));
	}

	@Override
	public String deleteTyre(int id) throws Exception {
		Tyre tyre=tyreRepo.findById(id).orElseThrow(()->new Exception("Tyre doesn't exist"));
		tyreRepo.delete(tyre);
		
		return "Tyre deleted successfully";
	}

	@Override
	public TyreDto addTyre(TyreDto tyreDto) {
		return convertTyreToTyreDto(tyreRepo.save(convertTyreDtoToTyre(tyreDto)));
	}

	@Override
	public TyreDto getTyre(int tyreId) throws Exception {
		return convertTyreToTyreDto(tyreRepo.findById(tyreId).orElseThrow(()-> new Exception("Tyre doesn't exist"))
		);
	}

	@Override
	public List<TyreDto> getAllTyres() throws Exception {
		List<Tyre> tyres= tyreRepo.findAll();
		if(tyres.size()==0)
			throw new Exception("No tyres present");
		return tyres.stream().map(t->convertTyreToTyreDto(t)).collect(Collectors.toList());
	}
  ///// just testing
}
