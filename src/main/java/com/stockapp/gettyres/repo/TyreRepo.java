package com.stockapp.gettyres.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stockapp.gettyres.entity.Tyre;

@Repository
public interface TyreRepo extends JpaRepository<Tyre, Integer>{

}
