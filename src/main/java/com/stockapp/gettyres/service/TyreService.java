package com.stockapp.gettyres.service;

import java.util.List;

import com.stockapp.gettyres.tyredto.TyreDto;


/**
 * @author Divya
 *
 */
public interface TyreService {
	
	/**
	 * @param list of tyres
	 * @return list of saved tyres
	 */
	public List<TyreDto> addTyres(List<TyreDto> tyresDto);
	
	
	/**
	 * @param tyre
	 * @return updated tyre
	 * @throws Tyre not found exception
	 */
	public TyreDto updateTyre(TyreDto tyreDto) throws Exception;
	
	/**
	 * @param tyre to be deleted
	 * @return deleting status
	 * @throws Tyre not found exception
	 */
	public String deleteTyre(int id) throws Exception;
	
	/**
	 * @param tyre
	 * @return saved tyre
	 */
	public TyreDto addTyre(TyreDto tyreDto);
	
	/**
	 * @param tyreId
	 * @return searched tyre
	 * @throws Tyre not found
	 */
	public TyreDto getTyre(int tyreId) throws Exception;
	
	
	/**
	 * @return all existing tyres
	 * @throws No tyres present exception
	 */
	public List<TyreDto> getAllTyres() throws Exception;
	
}
